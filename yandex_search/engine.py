import logging
import os
import re
from typing import List, Dict
from urllib.parse import urlparse, urlencode

from bs4 import ResultSet, Tag
from lxml import html
from oracle.captcha_controller.rucaptcha_service import RucaptchaService

from patch_controller.patch_controller import TEST
from search.exceptions import SolveCapcha
from search.search_engine import SearchEngine

logger = logging.getLogger(__name__)

RUCAPTCHA_KEY = os.environ.get('RUCAPTCHA_KEY', '4a3ad97e390a7ab3e7fdeaca9f6f5bb6')


class YandexSearch(SearchEngine):
    SEARCH_URL = f'{"mock" if TEST else "https"}://yandex.ru/search'
    CAPTCHA_CHECK_URL = f'{"mock" if TEST else "https"}://yandex.ru/checkcaptcha'
    RESULT_SELECTOR = '#search-result > li > div > h2 > a'  # css selector
    TITLE_SELECTOR = 'div.organic__url-text'

    USERAGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) ' \
                'Chrome/83.0.4103.97 Safari/537.36'

    @classmethod
    def search(cls, query: str, num_of_pages=1) -> List[Dict[str, str]]:
        results = []
        logger.info(f'[YandexSearch] Starting Yandex search with query [{query}]')
        for page in range(0, num_of_pages, 1):
            try:
                r = cls._search_request(query, page)
            except SolveCapcha as e:
                logger.error(e)
                break

            if not r.status_code == 200:
                # TODO: Придумать реализацию получше
                logger.error(f"Bad response status: {r.status_code}")
                break

            parsed_page = cls._parse_page(r.text)
            logger.info(f'[YandexSearch] Successful parsing of [{page + 1}] page')
            results += cls._extract_results(parsed_page)
        return results

    @classmethod
    def _search_request(cls, query, page):
        payload = {'text': query}
        if page > 0:
            payload['p'] = page  # нумерация страниц начинается с нуля
        cls.session.headers.update({'User-Agent': cls.USERAGENT})
        r = cls.session.get(f'{cls.SEARCH_URL}?{urlencode(payload)}')

        if urlparse(r.url).path == '/showcaptcha':
            logger.info('Try to solve yandex captcha.')
            r = cls._solve_yandex_captcha(r)
            logger.info('Captcha solved.')
        return r

    @classmethod
    def _extract_results(cls, results: ResultSet) -> List[Dict[str, str]]:
        parsed_results = []
        for result in results:
            try:
                title = result.select(cls.TITLE_SELECTOR)[0].text
            except IndexError:
                # ignore advertising
                continue

            url = cls._extract_link(result)
            parsed_results.append({'title': title, 'url': url})
        return parsed_results

    @staticmethod
    def _extract_link(result: Tag) -> str:
        href = result.get('href')
        return href

    @classmethod
    def _solve_yandex_captcha(cls, response):
        solver = RucaptchaService(RUCAPTCHA_KEY)
        img, key, retpath = cls._get_yandex_captcha_creds(link=response.url)
        if all([img, key, retpath]):
            captcha_answer = '123' if TEST else solver.solve_simple_captcha(img)
            response = cls._submit_captcha(captcha_answer=captcha_answer, key=key, retpath=retpath)
        else:
            raise SolveCapcha(f'Request was blocked. Solve capcha: {response.url}', response.url,
                              response.cookies.get_dict())
        return response

    @classmethod
    def _get_yandex_captcha_creds(cls, link: str):
        logger.info('Initialising Yandex captcha creds')
        resp = cls.session.get(link)
        html_page = html.fromstring(resp.text)
        img_link = html_page.xpath('//*[@class="captcha__image"]/img')[0].attrib.get('src')
        retpath = re.search(r'<input[^>]*name=\"retpath\"[^>]*value=\"([^>]*)\"', resp.text)
        key = re.search(r'<input[^>]*name=\"key\"[^>]*value=\"([^>]*)\"', resp.text)
        if all([key, retpath, img_link]):
            key = key.group(1)
            retpath = retpath.group(1)
            logger.info(f'Got [key: {key}][captcha image: {img_link}][retpath: {retpath}]')
            return img_link, key, retpath
        else:
            logger.error('Can not init captcha key')
            return None, None, None

    @classmethod
    def _submit_captcha(cls, captcha_answer: str, key: str, retpath: str):
        payload = {
            'key': key,
            'retpath': retpath,
            'rep': captcha_answer,
        }
        url = f'{cls.CAPTCHA_CHECK_URL}?{urlencode(payload)}'
        return cls.session.get(url)
