import asyncio
import logging
from collections import OrderedDict
from typing import Union, List, Dict

import aiohttp
from html2text import html2text, HTML2Text

from google_search.engine import GoogleSearch
from search.search_engine import SearchEngine
from yandex_search.engine import YandexSearch

logger = logging.getLogger(__name__)


class SearchController:
    def __init__(self, engine_choice: Union[List[SearchEngine], None] = None):
        """
        :param engine_choice: список поисковых систем, с помощью которых необходимо выполнить запрос или None.
        """
        if not engine_choice:
            self.search_engine = [GoogleSearch, YandexSearch]
        else:
            self.search_engine = []
            for e in engine_choice:
                if issubclass(e, SearchEngine):
                    self.search_engine.append(e)

        self.__aggregated_result_list = None

    def search(self, query: str, num_of_pages: int = 1) -> Dict[str, List[Dict[str, str]]]:
        """
        Метод осуществляет запрос к поисковому сервису
        :param query: Поисковой запрос
        :param num_of_pages: кол-во страниц поисковой выдачи, которые необходимо обработать
        :return: Словарь, ключи которого соответсвуют названиям классов поискового движка, а в значениях хранятся списки
        с результами поиска
        """
        search_result = {}
        for engine in self.search_engine:
            if issubclass(engine, SearchEngine):
                e = engine()
                search_result[engine.__name__] = e.search(query, num_of_pages)
                if not search_result.get(engine.__name__):
                    logger.warning(f'[{engine.__name__}] No results found for query [{query}]')
        return search_result

    def aggregate(self, search_result: Dict[str, List[Dict[str, str]]]) -> List[Dict[str, List[str]]]:
        """
        Конвертирует результаты в список вида [{'title': 'Title', 'url': 'URL', 'engine': ['engine1', 'engine2']}]
        :param search_result: результаты поиска, получаемые при помощи метода `search`
        :return: список вида [{'title': 'Title', 'url': 'URL', 'engine': ['engine1', 'engine2']}]
        """
        aggregator = OrderedDict()  # для учёта порядка. Первый engine в приоритете
        for sear_engine in self.search_engine:
            for result in search_result[sear_engine.__name__]:
                if result['url'] in aggregator:
                    aggregator[result['url']]['engine'].append(sear_engine.__name__)
                else:
                    aggregator[result['url']] = {**result, 'engine': [sear_engine.__name__]}

        self.__aggregated_result_list = [aggregator[item] for item in aggregator]
        return self.__aggregated_result_list

    async def get(self, search_item):
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(url=search_item['url']) as response:
                    html_response = await response.text()
                    search_item['html_response'] = html_response
                    search_item['html_text'] = self.__html2text(html_response, search_item)
                    search_item['status'] = 'ok'
                    return search_item
        except Exception as e:
            search_item['status'] = 'error'
            search_item['error_message'] = f"Unable to get url {search_item['url']} due to {e.__class__} e={e}."
            return search_item

    @staticmethod
    def __html2text(html_response: str, search_item: Dict) -> Union[str, None]:
        try:
            text = html2text(html_response, search_item['url'])
        except AssertionError:
            logger.warning(f"Try parse {search_item['url']} with ignore images")
            h = HTML2Text(baseurl=search_item['url'])
            h.ignore_images = True
            text = h.handle(html_response)
        except Exception:
            text = None
            logger.error(f"Can't get text from html for {search_item['url']}.",
                         exc_info=True)
        return text

    async def __get_text_pages(self):
        if self.__aggregated_result_list is None:
            return

        ret = await asyncio.gather(*[self.get(item) for item in self.__aggregated_result_list])
        return ret

    def get_pages(self):
        """
        производит скачивание страниц поисковой выдачи
        :return:
        """
        async_res = asyncio.run(self.__get_text_pages())
        return async_res
