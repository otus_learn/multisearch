from abc import ABC, abstractmethod, ABCMeta
from logging import getLogger
from typing import List, Dict

from bs4 import BeautifulSoup, ResultSet
from requests import Session

from patch_controller.patch_controller import PatchController, TEST

logger = getLogger(__name__)


class SearchEngine(ABC, metaclass=ABCMeta):
    SEARCH_URL = f'{"mock" if TEST else "https"}://ENGINE/search'
    RESULT_SELECTOR = 'css selector'  # css selector
    TITLE_SELECTOR = 'css selector'

    __pc = PatchController()
    session = Session()
    session.mount('mock', __pc.adapter)

    @abstractmethod
    def search(self, query: str, num_of_pages: int = 1) -> List[Dict[str, str]]:
        """
        Метод возвращает список словарей, в которых содержится title и url поисковой выдачи.
        :param query: Поисковый запрос
        :type query: str

        :param num_of_pages: количество страниц поисковой выдачи
        :param num_of_pages: int

        :return: Список вида [{'title': 'Title', 'url': 'https://dname.com;}]
        """
        pass

    @classmethod
    def _parse_page(cls, text: str) -> ResultSet:
        bs = BeautifulSoup(text, 'lxml')
        results = bs.select(cls.RESULT_SELECTOR)  # use css selector
        if not results:
            logger.error("No results found")
        return results
