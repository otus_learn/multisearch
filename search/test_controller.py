import unittest
from unittest import TestCase

import requests

from google_search.engine import GoogleSearch
from search.controller import SearchController
from yandex_search.engine import YandexSearch


class TestGoogleSearchController(TestCase):
    def setUp(self) -> None:
        self.searcher = SearchController([GoogleSearch])

    def test_search_google(self):
        search_result = self.searcher.search('Python')
        self.assertIn('GoogleSearch', search_result)
        self.assertEqual(len(search_result['GoogleSearch']), 10)
        self.assertEqual(search_result['GoogleSearch'][0]['title'], 'Welcome to Python.org')
        self.assertEqual(search_result['GoogleSearch'][0]['url'], 'https://www.python.org/')

    def test_search_many_pages_google(self):
        search_result = self.searcher.search('Python', num_of_pages=2)
        self.assertIn('GoogleSearch', search_result)
        self.assertEqual(len(search_result['GoogleSearch']), 20)
        self.assertEqual(search_result['GoogleSearch'][19]['title'],
                         'Google IT Automation with Python Professional Certificate | Coursera')
        self.assertEqual(search_result['GoogleSearch'][19]['url'],
                         'https://www.coursera.org/professional-certificates/google-it-automation')


class TestYandexSearchController(TestCase):
    def setUp(self) -> None:
        self.searcher = SearchController([YandexSearch])

    def test_search_yandex(self):
        search_result = self.searcher.search('Python')
        self.assertIn('YandexSearch', search_result)
        self.assertEqual(len(search_result['YandexSearch']), 16)
        self.assertEqual(
            search_result['YandexSearch'][0]['title'],
            'Онлайн-курс: Язык Python с нуля – От Mail.Ru Group'
        )
        self.assertEqual(
            search_result['YandexSearch'][0]['url'],
            'http://yabs.yandex.ru/count/WaOejI_zOFK1vH40b1qeltIY0J-baWK0zG8GW8Y00J4iA2bV000003W40d2Cx9UWf'
            'mw00G680O_fkPa-a07yij7gCvW1WAwGqZUu0UpykhCSm05Ss07GpVKOu07KXyiEw05oe0BKw-WIm0BDxeoFrPZcdFwihy'
            'Vx2vW3eTaagku1-0JfjtU81UctTv05ZUFj0w05YhWag0NUhXwm1Twk7hW5oyHzm0MDu-q3o0NBn7q4g0O4oGOia9X5LTm'
            'RG0FrgQmaHo4BFh07W82O3BW7j0RG1mRO1mVW1uOAyGUgbONVjeSAv8I2a07W2Egrj0c02W6O2Z39gWiG2QQJuVMf001U'
            '9qZ12Qm50DaBw0lfjtVm2mQ83EYAthu1gGmmQWlGnc9aF-WCA-0DWu20G8aE-pfmpeZIxAoWugpKYlUDs3qArwDmFVO__'
            'Qx65CWGe2ZG4CANhry3a141c17qomQX4SmYPFGOrzrfFx4IxRF16soqvJ_m4XC1u1FBn7sW5Cl4VQWKZUFj0y0KWD3IfD'
            'tHyGNe50p85Ocoqvm5q1MatT7n1TWLmOhsxAEFlFnZy9WMqD7Y-0MW5j3reVa5oHRG5kYAthu1WHUO5vIdsG-e5md05mp'
            'O5y24FPaOe1W9i1ZPcQJv1T0O8Hu0VP90GoJw8q0k6paXA2ON07WBN97p3c6YGm9U104RCJedH5ibM7eP00ZnUxKPMEOd'
            '62Ck4a3q7ebxHfKWMzuA3En0az63Us56nw14FK1tq5SzEFslCgvAu8qUXo8JC0y0~1?from=yandex.ru%3Bsearch%26'
            '%23x2F%3B%3Bweb%3B%3B0%3B&q=python&etext=2202.3lUK2HNJFQ6GE_NpURGnCGphYnJzZXdiemNuc3N3aXQ.d07'
            '5983c1767386a1bc9426ae5f285fcb86e4048'
        )

    def test_search_many_pages_yandex(self):
        search_result = self.searcher.search('Python', num_of_pages=2)
        self.assertIn('YandexSearch', search_result)
        self.assertEqual(len(search_result['YandexSearch']), 31)
        self.assertEqual(
            search_result['YandexSearch'][30]['title'],
            'Программируем на Python / ozon.ru'
        )
        self.assertEqual(
            search_result['YandexSearch'][30]['url'],
            'http://yabs.yandex.ru/count/WaaejI_zOEu1_H40L1ueqQV17szDUGK0xW8GW0WnvIWfNm00000u1404I0RQik_ih'
            'IE00G680VALGv01Zi3h_Ptbejy7e064pBRCme20W0Ae0OJCjin2k062wEc97y01NDW1ujNY7k01hlRh7UW1cWAO0kmCe0'
            'Ba3C02yfL3s084y0AihyVx2vW3i8W3e0C6g0CQq1du18QD28W5Xeq8a0NwcGYW1RAy0gW5r8S2i0NKXmAu1VYA1AW61Ca'
            '6B92OHLNS6q3H1YmaphnLt1j0sGO0003GzLoegK9D2pwm1u20c3ou1xG6q0SEs0S7u0U62l47gfM5txQ72kI4Wf21nB86'
            'W0e1c0hvt2h92lCXG_9XxElu2e2r6AeB41sBXO_rN000CWX7p0ci1G3P2-WBXeq8y0i6Y0pcZjw-0QaC-Bp9jIQHqx_e3'
            '2lW3OE0W4293bgU1tTdxQO_a0xUtpkO3goWugpKYlUDsDaFu0y1W13YlxSO4u0H59WHzCi6-Z5JnTwq6p_m4XC1u1FuYW'
            'AW5FYA0gWK-fdScBtq1UWK3CWLvggK_902q1NauzVu1TWLmOhsxAEFlFnZy9WMqD7Y-0MW5j3reVa5oHRG5kQEthu1WHU'
            'O5wVrqoEe5md05mpO5y24FPaOe1WHi1ZPcQJv1T0O8Hi0VP90GoGqIi1SDh9GN7iQ07W0xcKP9EvqI0Gqmkigme54bUmK'
            '7i53V4q1mjr7mGne7yXeBXSvNDuTP6r9av5diPt1umDq1uIH3FE0wXJE6QoC7uSym3m0~1?from=yandex.ru%3Bsearc'
            'h%26%23x2F%3B%3Bweb%3B%3B0%3B&q=python&etext=2202.1FlzxCNA2HQ17k4dV_WMlmV4a3RzeGVibG51eWp3bGQ'
            '.a11e45124a85da119196730b3423070db39c944a'
        )

    def test_solve_yandex_captcha(self):
        request = requests.Request(
            'GET',
            'mock://yandex.ru/showcaptcha?cc=1&retpath=https%3A//yandex.ru/search'
            '%3Ftext%3D1%2BPython_61f9ea469ccaa7a22029ba527db8545e&t=0/1597656875'
            '/2185b61f9a5410ab3697c339262cee70&s=2f64c353eab0b06a22927c045e59fb4b'
        )
        resp = YandexSearch._solve_yandex_captcha(request)
        parsed_page = YandexSearch._parse_page(resp.text)
        result = YandexSearch._extract_results(parsed_page)
        self.assertEqual(result[0].get('title'), 'Онлайн-курс: Язык Python с нуля – От Mail.Ru Group')


class TestSearchController(TestCase):
    def setUp(self) -> None:
        self.searcher = SearchController()

    def test_search(self):
        search_result = self.searcher.search('Python', num_of_pages=2)
        aggregated_result = self.searcher.aggregate(search_result)
        self.assertEqual(len(aggregated_result), 44)

    def test_get_text_pages(self):
        search_result = self.searcher.search('Python', num_of_pages=2)
        self.searcher.aggregate(search_result)
        pages = self.searcher.get_pages()
        self.assertEqual(len(pages), 44)
        self.assertTrue({'title', 'url', 'html_text'} <= pages[0].keys())


if __name__ == '__main__':
    unittest.main()
