class ErrorSearchResponse(Exception):
    pass


class BadResultSelector(Exception):
    pass


class SolveCapcha(Exception):
    def __init__(self, message, url, cookies):
        self.message = message
        self.url = url
        self.cookies = cookies
