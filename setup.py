import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

install_requires = [
    'requests',
    'lxml',
    'bs4',
    'requests-mock',
    'aiohttp',
    'html2text',

]

setuptools.setup(
    name="multisearch",
    version=f"0.0.0.local",
    author="Alex Kharuk",
    author_email="a.haruk@gmail.com",
    description="Library for use search engines",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.lan/filigree/multisearch",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    install_requires=install_requires,
    include_package_data=True,
)