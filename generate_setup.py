import os

from jinja2 import Template


def generate():
    with open('setup.py.skeleton', 'r') as skeleton_file:
        skeleton = skeleton_file.read()
        template = Template(skeleton)

    with open('requirements/requirements.in') as requirements_file:
        requirements = requirements_file.read().split('\n')

    build = os.environ.get('CI_PIPELINE_ID', '0.local')
    setup = template.render(build=build, requirements=requirements)

    with open('setup.py', 'w') as setup_file:
        setup_file.write(setup)


if __name__ == '__main__':
    generate()
