# multisearch

Библиотека для работы с поисковыми системами.

## Структура проекта

В каталоге `requirements` расположены зависимости для разработки, тестирования и непосредственного функционирования 
библиотеки.

`requirements/dev.in` - зависимости, необходимые для разработки и сборки.

`requirements/requirements.in` - зависимости библиотеки.

`requirements/testing.in` - зависимости, необходимые для запуска тестов.

Для актуализации `setup.py` необходимо воспользоваться скриптом `generate_setup.py`

Пакет `search` - базовый пакет, организующий работу с поисковыми системами, поддерживаемыми данной библиотекой.

Пакеты `google_search` и `yandex_search` реализуют работу с поисковыми системами google и yandex соответственно.

Пакет `patch_controller` содержит реализацию патча запросов и содержит mock ответов от поисковых систем.

## Описание работы

Библиотека работает с google и yandex.

Пример работы:

```python
from pprint import pprint

from search.controller import SearchController

if __name__ == '__main__':
    searcher = SearchController()
    res = searcher.search(input('input query search: '))
    searcher.aggregate(res)
    pprint(searcher.get_pages())

```

Метод `search` производит поиск в поисковых системах и объединяет результаты поиска по типу поисковой системы.
Метод `aggregate` преобразует результаты поисковой выдачи. В результирующем списке каждый URL встречается только один
раз. В поле `engine` хранится список, указывающий на то, какие поисковые системы вернули данный URL.
Метод `get_pages` производит скачивание страниц поисковой выдачи, дополняет результаты полями `html_response` с 
полученным HTML и `html_text`, преобразованным в текст HTML. 

Пример вывода:

```python
[{'engine': ['GoogleSearch', 'YandexSearch'],
  'title': 'Welcome to Python.org',
  'url': 'https://www.python.org/',
  'html_response': '<!DOCTYPE html>',
  'html_text': ''},
 {'engine': ['GoogleSearch', 'YandexSearch'],
  'title': 'Python — Википедия',
  'url': 'https://ru.wikipedia.org/wiki/Python',
  'html_response': '<!DOCTYPE html>',
  'html_text': ''},
...
]
```

## Запуск тестов

Для запуска тестов необходимо выполнить команду 

```shell script

TEST=True python -m unittest
```

Значение переменной среды окружения `TEST=True` указывает на необходимость использования mock вместо выполнения реальных
запросов. 
