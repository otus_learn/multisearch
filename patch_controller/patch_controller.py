import os
import sysconfig
from pathlib import Path
from urllib.parse import urlparse, urlunparse

import pkg_resources
import requests_mock

from patch_controller.exception import BadMock

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEST = os.environ.get('TEST', 'false').casefold().strip() == 'true'
# Для определения того, где искать файлы patchdata
if 'multisearch' in {pkg.key for pkg in pkg_resources.working_set}:
    # Если пакет установлен, то префиксом будет являться каталог установки интерпретатора
    DATA_PREFIX = sysconfig.get_paths()["purelib"]
else:
    # Если пакет НЕ установлен, то префиксом будет являться корневой каталог проекта
    DATA_PREFIX = BASE_DIR


class PatchController:
    def __init__(self):
        self.adapter = requests_mock.Adapter()

        if not TEST:
            return

        self.adapter.register_uri(
            'GET',
            'mock://google.com/search?q=Python',
            text=self.__load_google_data,
        )

        self.adapter.register_uri(
            'GET',
            'mock://www.google.com/search?q=Python&start=10',
            text=self.__load_google_data,
        )

        self.adapter.register_uri(
            'GET',
            'mock://yandex.ru/search?text=Python',
            text=self.__load_yandex_data,
        )

        self.adapter.register_uri(
            'GET',
            'mock://yandex.ru/checkcaptcha?key=00AEW47rK9clmqsVVIyQVI24tTck4Ino_0%2F1597660795'
            '%2F25970aa769cb141410eb3f7e2146421f_a042524237e4d114e24cf01107a4e078&retpath=https%3A%2F%2Fyandex.ru'
            '%2Fsearch%3Ftext%3D1513%26amp%3Blr%3D2%26amp%3Bredircnt%3D1597660795.1_1ad49fab147dee3ac0cd33401c46f923'
            '&rep=123',
            text=self.__load_yandex_data,
        )

        self.adapter.register_uri(
            'GET',
            'mock://yandex.ru/showcaptcha?cc=1&retpath=https%3A//yandex.ru/search'
            '%3Ftext%3D1%2BPython_61f9ea469ccaa7a22029ba527db8545e&t=0/1597656875'
            '/2185b61f9a5410ab3697c339262cee70&s=2f64c353eab0b06a22927c045e59fb4b',
            text=self.__load_yandex_data,
        )

        self.__register_results_uri()

    def __register_results_uri(self):
        mock_dir_path = Path(DATA_PREFIX, 'patch_controller', 'patchdata', 'mock')
        index_path = Path(mock_dir_path, 'index.txt')

        with open(index_path) as index_file:
            index_list = index_file.read().splitlines()
            for line in index_list:
                mock_file_name, mock_url = line.split('\t')
                mock_url = urlunparse(urlparse(mock_url)._replace(scheme='mock'))
                mock_file_path = Path(mock_dir_path, f'{mock_file_name}.html')
                with open(mock_file_path, 'r') as mock_file:
                    patch = mock_file.read()
                    self.adapter.register_uri(
                        'GET',
                        mock_url,
                        text=patch,
                    )

    @staticmethod
    def __load_google_data(*args, **kwargs):
        if args[0].qs == {'q': ['python']}:
            path = Path(DATA_PREFIX, 'patch_controller', 'patchdata', 'google_python.html')
        elif args[0].qs == {'q': ['python'], 'start': ['10']}:
            path = Path(DATA_PREFIX, 'patch_controller', 'patchdata', 'google_python_10.html')
        else:
            raise BadMock

        with open(path) as path_file:
            patch = path_file.read()
        return patch

    @staticmethod
    def __load_yandex_data(*args, **kwargs):
        path = Path(DATA_PREFIX, 'patch_controller', 'patchdata')
        if args[0].qs == {'text': ['python']} or args[0].qs.get('retpath') == [
            'https://yandex.ru/search?text=1513'
            '&amp;lr=2&amp;redircnt=1597660795.1_1'
            'ad49fab147dee3ac0cd33401c46f923'
        ]:
            path = path.joinpath('yandex_python.html')
        elif args[0].qs == {'text': ['python'], 'p': ['1']}:
            path = path.joinpath('yandex_python_1.html')
        elif args[0].qs.get('s') == ['2f64c353eab0b06a22927c045e59fb4b']:
            path = path.joinpath('yandex_captcha.html')
        else:
            raise BadMock

        with open(path) as path_file:
            patch = path_file.read()
        return patch
