from search.exceptions import ErrorSearchResponse


class ErrorGoogleSearchResponse(ErrorSearchResponse):
    pass
