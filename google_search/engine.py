from logging import getLogger
from typing import List, Dict
from urllib.parse import urlparse, parse_qs, urlencode

from bs4 import ResultSet, Tag
from requests import Response

from patch_controller.patch_controller import TEST
from search.search_engine import SearchEngine

logger = getLogger(__name__)


class GoogleSearch(SearchEngine):
    SEARCH_URL = f'{"mock" if TEST else "https"}://google.com/search'
    RESULT_SELECTOR = '#main > div > div > div:nth-child(1) > a'  # css selector
    TITLE_SELECTOR = 'h3 > div'

    @classmethod
    def search(cls, query: str, num_of_pages: int = 1) -> List[Dict[str, str]]:
        results = []
        logger.info(f'[GoogleSearch] Starting Google search with query [{query}]')
        for page in range(0, num_of_pages, 1):
            logger.info(f'[GoogleSearch] Page [{page + 1}/{num_of_pages}]')
            r = cls._search_request(query, page)

            if not r.status_code == 200:
                # TODO: Придумать реализацию получше
                logger.error(f"Bad response status: {r.status_code}")
                break

            parsed_page = cls._parse_page(r.text)
            logger.info(f'[GoogleSearch] Successful parsing of [{page + 1}] page')
            results += cls._extract_results(parsed_page)
        return results

    @classmethod
    def _search_request(cls, query: str, page: int) -> Response:
        payload = {'q': query}

        if page > 0:
            payload['start'] = page * 10

        r = cls.session.get(f'{cls.SEARCH_URL}?{urlencode(payload)}')
        return r

    @classmethod
    def _extract_results(cls, results: ResultSet) -> List[Dict[str, str]]:
        parsed_results = []
        for result in results:
            try:
                title = result.select(cls.TITLE_SELECTOR)[0].text
            except IndexError:
                # ignore advertising in google
                continue

            url = cls._extract_link(result)
            parsed_results.append({'title': title, 'url': url})
        return parsed_results

    @staticmethod
    def _extract_link(result: Tag) -> str:
        href = result.get('href')
        return parse_qs(urlparse(href).query)['q'][0]
